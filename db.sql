-- public.sec_apps definition

-- Drop table

-- DROP TABLE sec_apps;

CREATE TABLE sec_apps (
	app_name varchar(50) NOT NULL,
	description varchar(100) NULL,
	is_door bool NOT NULL DEFAULT false,
	"type" varchar(15) NOT NULL DEFAULT 'grid'::character varying,
	CONSTRAINT sec_apps_pk PRIMARY KEY (app_name)
);


-- public.sec_groups definition

-- Drop table

-- DROP TABLE sec_groups;

CREATE TABLE sec_groups (
	id_group serial NOT NULL,
	description varchar(100) NULL,
	CONSTRAINT sec_groups_pk PRIMARY KEY (id_group)
);


-- public.sec_users definition

-- Drop table

-- DROP TABLE sec_users;

CREATE TABLE sec_users (
	login varchar(20) NOT NULL,
	"password" varchar(200) NOT NULL,
	active bool NULL DEFAULT true,
	email varchar(30) NOT NULL,
	"name" varchar(30) NULL,
	CONSTRAINT sec_users_pk PRIMARY KEY (login),
	CONSTRAINT sec_users_un UNIQUE (email)
);


-- public.test definition

-- Drop table

-- DROP TABLE test;

CREATE TABLE test (
	id serial NOT NULL,
	nombre varchar(100) NULL,
	CONSTRAINT test_pk PRIMARY KEY (id)
);


-- public.sec_groups_apps definition

-- Drop table

-- DROP TABLE sec_groups_apps;

CREATE TABLE sec_groups_apps (
	app_name varchar(50) NOT NULL,
	id_group int4 NOT NULL,
	priv_access bool NULL DEFAULT false,
	priv_insert bool NULL DEFAULT false,
	priv_update bool NULL DEFAULT false,
	priv_delete bool NULL DEFAULT false,
	priv_report bool NULL DEFAULT false,
	CONSTRAINT sec_groups_apps_pk PRIMARY KEY (id_group, app_name),
	CONSTRAINT sec_groups_apps_fk FOREIGN KEY (id_group) REFERENCES sec_groups(id_group) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT sec_groups_apps_fk_1 FOREIGN KEY (app_name) REFERENCES sec_apps(app_name) ON UPDATE CASCADE ON DELETE CASCADE
);


-- public.sec_users_groups definition

-- Drop table

-- DROP TABLE sec_users_groups;

CREATE TABLE sec_users_groups (
	login varchar(20) NOT NULL,
	id_group int4 NOT NULL,
	CONSTRAINT sec_users_groups_pk PRIMARY KEY (id_group, login),
	CONSTRAINT sec_users_groups_fk FOREIGN KEY (login) REFERENCES sec_users(login) ON UPDATE CASCADE ON DELETE CASCADE,
	CONSTRAINT sec_users_groups_fk_1 FOREIGN KEY (id_group) REFERENCES sec_groups(id_group) ON UPDATE CASCADE ON DELETE CASCADE
);




CREATE OR REPLACE FUNCTION public.fn_user_groups(login_param character varying)
 RETURNS TABLE(id_group integer, description character varying, belongs boolean)
 LANGUAGE plpgsql
AS $function$
begin
	return query
		select sg.id_group, sg.description,
		(select true from sec_users_groups sug where sug.login = login_param and sug.id_group = sg.id_group) as belongs
		from sec_groups sg;
end;
$function$
;

CREATE OR REPLACE FUNCTION public.new_app()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$ 
	begin
	if new.is_door  then
		insert into public.sec_groups_apps (app_name, id_group)
		select new.app_name, id_group from public.sec_groups sg;
	else
		if new."type" = 'form' then 
			insert into public.sec_groups_apps (app_name, id_group, priv_access, priv_insert, priv_update, priv_delete, priv_report)
			select new.app_name, id_group, true, true, true, true, true from public.sec_groups sg;
		else
			insert into public.sec_groups_apps (app_name, id_group, priv_access)
			select new.app_name, id_group, true from public.sec_groups sg;
		end if;
		
	end if;
	return new;
	end;
$function$
;

-- public.vw_user_permissions source

CREATE OR REPLACE VIEW public.vw_user_permissions
AS SELECT sga.app_name,
    sug.login,
    ( SELECT sga2.priv_access
           FROM sec_groups_apps sga2
             JOIN sec_users_groups sug2 ON sga2.id_group = sug2.id_group
          WHERE sga2.app_name::text = sga.app_name::text AND sug2.login::text = sug.login::text AND sga2.priv_access
         LIMIT 1) AS priv_access,
    ( SELECT sga3.priv_delete
           FROM sec_groups_apps sga3
             JOIN sec_users_groups sug3 ON sga3.id_group = sug3.id_group
          WHERE sga3.app_name::text = sga.app_name::text AND sug3.login::text = sug.login::text AND sga3.priv_delete
         LIMIT 1) AS priv_delete,
    ( SELECT sga4.priv_insert
           FROM sec_groups_apps sga4
             JOIN sec_users_groups sug4 ON sga4.id_group = sug4.id_group
          WHERE sga4.app_name::text = sga.app_name::text AND sug4.login::text = sug.login::text AND sga4.priv_insert
         LIMIT 1) AS priv_insert,
    ( SELECT sga5.priv_report
           FROM sec_groups_apps sga5
             JOIN sec_users_groups sug5 ON sga5.id_group = sug5.id_group
          WHERE sga5.app_name::text = sga.app_name::text AND sug5.login::text = sug.login::text AND sga5.priv_report
         LIMIT 1) AS priv_report,
    ( SELECT sga6.priv_update
           FROM sec_groups_apps sga6
             JOIN sec_users_groups sug6 ON sga6.id_group = sug6.id_group
          WHERE sga6.app_name::text = sga.app_name::text AND sug6.login::text = sug.login::text AND sga6.priv_update
         LIMIT 1) AS priv_update
   FROM sec_groups_apps sga
     JOIN sec_users_groups sug ON sga.id_group = sug.id_group;


-- Table Triggers

create trigger tg_new_app after
insert
    on
    public.sec_apps for each row execute function new_app();




-- Tables contents

INSERT INTO public.sec_apps (app_name,description,is_door,"type") VALUES
	 ('/page-two','Page Two',true,'grid'),
	 ('/test','Test',true,'grid'),
	 ('/users','Users List',true,'grid'),
	 ('/groups','Groups List',true,'grid'),
	 ('/error','Error Page',false,'blank'),
	 ('/groups/form','Groups Form',true,'form'),
	 ('/apps','Application List',true,'grid'),
	 ('/apps/form','Application Form',true,'form'),
	 ('/logout','Log Out RunTime',false,'backend'),
	 ('/','Main Page',false,'dashboard'),
	 ('/users/form','Users List',true,'form');


INSERT INTO public.sec_groups (description) VALUES
	 ('Administrador'),
	 ('Recepcion');

INSERT INTO public.sec_users (login,"password",active,email,"name") VALUES
	 ('Junior','$2b$10$hHEpY6rpB17nB0jiVNAYR.GAcJsxXsClA.B54HaDAYlFBr88N0wIi',true,'juninogarozzo@gmail.com','Junior'),
	 ('nombre','$2b$10$hHEpY6rpB17nB0jiVNAYR.GAcJsxXsClA.B54HaDAYlFBr88N0wIi',true,'algo@algo.com','nombre');


INSERT INTO public.sec_users_groups (login,id_group) VALUES
	 ('Junior',1),
	 ('nombre',2);
	 	 
INSERT INTO public.sec_groups_apps (app_name,id_group,priv_access,priv_insert,priv_update,priv_delete,priv_report) VALUES
	 ('/page-two',1,true,true,true,true,true),
	 ('/test',1,true,true,true,true,true),
	 ('/test',2,false,false,false,false,false),
	 ('/',1,true,true,true,true,true),
	 ('/',2,true,true,true,true,true),
	 ('/groups/form',2,true,true,false,true,true),
	 ('/groups/form',1,true,true,true,true,false),
	 ('/apps',2,false,false,false,false,false),
	 ('/error',1,true,true,true,true,true),
	 ('/error',2,true,true,true,true,true),
	 ('/logout',1,true,true,true,true,true),
	 ('/logout',2,true,true,true,true,true),
	 ('/apps/form',2,false,false,false,false,false),
	 ('/page-two',2,false,false,false,false,false),
	 ('/apps',1,true,false,false,false,false),
	 ('/apps/form',1,true,true,true,true,false),
	 ('/users',1,true,true,true,true,true),
	 ('/users/form',2,false,false,false,false,false),
	 ('/users/form',1,true,true,true,true,false),
	 ('/users',2,true,false,false,true,false),
	 ('/groups',1,true,true,true,true,true),
	 ('/groups',2,true,false,false,false,false);


CREATE OR REPLACE FUNCTION public.new_group()
 RETURNS trigger
 LANGUAGE plpgsql
AS $function$ 
	begin
		insert into public.sec_groups_apps (app_name, id_group, priv_access)
		select app_name, new.id_group, true from public.sec_apps;
	return new;
	end;
$function$
;

create trigger tg_new_group after
insert
    on
    public.sec_groups for each row execute function new_group();
