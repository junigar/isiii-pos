if (process.env.NODE_ENV === 'development') {
    require('dotenv').config()
}
const express = require('express')
const app = express()
const path = require('path')
const passport = require('passport')
const flash = require('express-flash')
const session = require('express-session')
const methodOverride = require('method-override')
const logger = require('morgan');
const { checkAuthenticated, checkNotAuthenticated } = require('./middleware/authenticate')


const usersModel = require('./models/sec_users')

const sec_usersRoutes = require('./routes/security/sec_usersRoutes')
const sec_groupsRoutes = require('./routes/security/sec_groupsRoutes')
const sec_groupAppsRoutes = require('./routes/security/sec_groups_appsRouter')

const initializePassport = require('./passport-config')
const PORT = process.env.PORT || 3000

initializePassport(
    passport,
    email => usersModel.getUserbyEmail(email),
    login => usersModel.getUserbyLogin(login)
)
app.use(flash())
app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(methodOverride('_method'))
app.use(logger('dev'));

app.use('/static', express.static(path.join(__dirname, 'public')));
app.set('view engine', 'ejs')
app.use(express.urlencoded({ extended: true }))

app.get('/login', checkNotAuthenticated, (req, res) => {
    res.render('login')
})

app.post('/login', checkNotAuthenticated, passport.authenticate('local', {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: true
}))

app.use(checkAuthenticated)

app.get('/', (req, res) => {
    res.render('index', { name: req.user.name })
})

app.use('/users', sec_usersRoutes)
app.use('/groups', sec_groupsRoutes)
app.use('/groups_apps', sec_groupAppsRoutes)

app.delete('/logout', (req, res) => {
    req.logOut()
    res.redirect('/login')
})

app.get('/error', (req, res) => {
    res.render('server/error-handler')
})

app.listen(PORT)