const { pool } = require('./pg-config')

const checkPermission = (route, login) => {
    const queryStr = `SELECT * 
                      FROM vw_user_permissions 
                      WHERE app_name = $1 
                      AND login = $2 LIMIT 1`;
    return  pool.query(queryStr, [route, login])
}


module.exports = {
    checkPermission
}