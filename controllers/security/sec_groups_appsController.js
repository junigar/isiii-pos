const groupsApps = require('../../models/security/sec_groups_appsModel')
const { checkPermission } = require('../../userPermission')
const { oneToMany } = require('../../util/queryConstructor')

const selectGroup = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login;
    let userPermissions;
    let returnObj = {}
    checkPermission(route, login)
        .then(
            (result) => {
                if (!result.rows)
                    throw new Error('Grid Route does not exist into DB')
                else {
                    userPermissions = result.rows[0]
                    if (!userPermissions.priv_access)
                        throw new Error('Not allowed to get.')
                    else
                        return groupsApps.getAllGroups()
                }
            })
        .then(
            (result) => {
                if (result) {
                    if (result.rows.length > 0)
                        returnObj.resultSet = result.rows
                    let action = req.query.action
                    if (action) 
                        returnObj.action = action
                    returnObj.app_type = 'form'
                    res.render('security/sec_groups_apps/grid_select_group', returnObj)
                } else {
                    throw new Error('Error getting groups.')
                }
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/')
            }
        )
}

const redirToList = (req, res) => {
    const idGroup = req.body.group

    if (idGroup)
        res.redirect(`/groups_apps/${idGroup}`)
    else
        res.redirect('/groups_apps')
}

const listGroupsAppsByIdGroup = (req, res) => {
    const route = '/groups_apps'
    const login = req.user.login
    const idGroup = req.params.id_group
    let userPermissions;
    checkPermission(route, login)
        .then(
            (result) => {
                if (!result.rows)
                    throw new Error('Grid Route does not exist into DB')
                else {
                    userPermissions = result.rows[0]
                    if (!userPermissions.priv_access)
                        throw new Error('Not allowed to get.')
                    else
                        return groupsApps.getGroupsAppsByIdGroup(idGroup)
                }
            })
        .then(
            (result) => {
                if (!result)
                    throw Error('Error getting apps.')
                else {
                    if (!(result.rows.length > 0))
                        throw new Error('No rows returned.')
                    else
                        res.render('security/sec_groups_apps/grid_group_apps', { resultSet: result.rows, idGroup })
                }
            })
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/groups_apps')
            })
}

const updatePermissions = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const idGroup = req.params.id_group
    let userPermissions;
    checkPermission(route, login)
        .then(
            (result) => {
                if (!result.rows)
                    throw new Error('Grid Route does not exist into DB')
                else {
                    userPermissions = result.rows[0]
                    if (!userPermissions.priv_access)
                        throw new Error('Not allowed to get.')
                    else {
                        if (!userPermissions.priv_update)
                            throw new Error('Not allowed to update.')
                        else {
                            return groupsApps.updateGroupApp(req.body, idGroup)
                        }
                    }
                }
            })
        .then(
            (result) => {
                if (!result)
                    throw new Error('Error updating rows.')
                else
                    res.redirect('/groups_apps?action=update')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/groups_apps')
            }
        )
}

module.exports = {
    selectGroup,
    redirToList,
    listGroupsAppsByIdGroup,
    updatePermissions
}