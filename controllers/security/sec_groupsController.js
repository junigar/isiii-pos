const group = require('../../models/security/sec_groupsModel')
const { checkPermission } = require('../../userPermission')

const getGroups = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    let userPermissions;
    let formUserPermission;

    checkPermission(route, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else
                    return checkPermission(`${route}/form`, login)
            })
        .then(
            (result) => {
                if (!result)
                    throw new Error('Form not found.')
                else {
                    formUserPermission = result.rows[0]
                    return group.getAllGroups()
                }
            })
        .then(
            (result) => {
                if (result) {
                    let action = req.query.action
                    let returnParams = {
                        resultSet: result.rows,
                        permissions: formUserPermission,
                        app_type: 'grid'
                    }
                    if (req.query.action) {
                        returnParams.action = req.query.action
                    }
                    res.render('security/sec_groups/grid_sec_groups', returnParams)
                }
                else
                    throw new Error('No rows selected.')
            })
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/groups')
            })
}

const editGroup = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const id_group = req.params.id_group
    let userPermissions;

    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else if (!userPermissions.priv_update)
                    throw new Error('Not allowed to update.')
                else {
                    const toUpdateGroup = new group.Group(id_group, req.body.description)
                    return group.updateGroup(toUpdateGroup)
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/groups?action=update')
                else
                    throw new Error('No row updated.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/groups')
            }
        )
}

const insertGroup = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    let userPermissions;
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else if (!userPermissions.priv_insert)
                    throw new Error('Not allowed to insert.')
                else {
                    const toInsertGroup = new group.Group('default', req.body.description)
                    return group.insertGroup(toInsertGroup)
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/groups?action=insert')
                else
                    throw new Error('No row updated.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/groups')
            }
        )
}

const getFormGroup = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const id_group = req.params.id_group
    let userPermissions;
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]

                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                if (id_group) {
                    if (userPermissions.priv_update)
                        return group.getGroupById(id_group)
                    else
                        throw new Error('Not allowed to update.')
                } else {
                    if (!userPermissions.priv_insert)
                        throw new Error('Not allowed to insert.')
                    else
                        return group.getGroupById(id_group)
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.render('security/sec_groups/form_sec_groups', { resultSet: result.rows[0], permissions: userPermissions })
                else
                    throw new Error('Error getting group.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/groups')
            }
        )
}

const deleteGroup = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    let userPermissions;

    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else if (!userPermissions.priv_delete)
                    throw new Error('Not allowed to delete.')
                else {
                    const toDeleteGroup = new group.Group(req.params.id_group, '')
                    return group.deleteGroup(toDeleteGroup)
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/groups?action=delete')
                else
                    throw new Error('No row deleted.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/groups')
            }
        )
}

module.exports = {
    getGroups,
    getFormGroup,
    editGroup,
    insertGroup,
    deleteGroup
}