const app = require('../../models/security/sec_appsModel')
const { checkPermission } = require('../../userPermission')

const getApps = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    let userPermissions;
    let formUserPermission;

    checkPermission(route, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else
                    return checkPermission(`${route}/form`, login)
            })
        .then(
            (result) => {
                if (!result)
                    throw new Error('Form not found.')
                else {
                    formUserPermission = result.rows[0]
                    return app.getAllApps()
                }
            })
        .then(
            (result) => {
                if (result) {
                    for(let row of result.rows){
                        row.app_name_encoded = encodeURIComponent(row.app_name)
                    }
                    let action = req.query.action
                    let returnParams = {
                        resultSet: result.rows,
                        permissions: formUserPermission,
                        app_type: 'grid'
                    }

                    if (action) {
                        returnParams.action = action
                    }
                    res.render('security/sec_apps/grid_sec_apps', returnParams)
                }
                else
                    throw new Error('No rows selected.')
            })
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/apps')
            })
}

const editApp = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const app_name = decodeURIComponent(req.params.app_name)
    let userPermissions;

    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else if (!userPermissions.priv_update)
                    throw new Error('Not allowed to update.')
                else {
                    const toUpdateGroup = new app.App(app_name, req.body.description, req.body.is_door, req.body.type)
                    return app.updateApp(toUpdateGroup)
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/apps?action=update')
                else
                    throw new Error('No row updated.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                red.redirect('/apps')
            }
        )
}

const insertApp = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    let userPermissions;
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else if (!userPermissions.priv_insert)
                    throw new Error('Not allowed to insert.')
                else {
                    const toInsertApp = new app.App(req.body.app_name, req.body.description, req.body.is_door, req.body.type)
                    return app.insertApp(toInsertApp)
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/apps?action=insert')
                else
                    throw new Error('No row updated.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/apps')
            }
        )
}

const getFormApp = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const app_name = decodeURIComponent(req.params.app_name)
    let userPermissions;
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                userPermissions = result.rows[0]

                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                if (app_name) {
                    if (userPermissions.priv_update)
                        return app.getAppByName(app_name)
                    else
                        throw new Error('Not allowed to update.')
                } else {
                    if (!userPermissions.priv_insert)
                        throw new Error('Not allowed to insert.')
                    else 
                        return app.getAppByName(app_name)
                }
            }
        )
        .then(
            (result) => {
                // console.log(result)
                if (result.rows[0]){
                    result.rows[0].app_name_encoded = encodeURIComponent(result.rows[0].app_name)
                    res.render('security/sec_apps/form_sec_apps', { resultSet: result.rows[0], permissions: userPermissions })
                }
                else
                    res.render('security/sec_apps/form_sec_apps', { permissions: userPermissions })
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/apps')
            }
        )
}

const deleteApp = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const app_name = decodeURIComponent(req.params.app_name)
    const login = req.user.login
    let userPermissions;
    console.log(app_name)
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                console.log(result)
                userPermissions = result.rows[0]
                if (!userPermissions.priv_access)
                    throw new Error('Not allowed to get.')
                else if (!userPermissions.priv_delete)
                    throw new Error('Not allowed to delete.')
                else {
                    const toDeleteApp = new app.App(app_name)
                    return app.deleteApp(toDeleteApp)
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/apps?action=delete')
                else
                    throw new Error('No row deleted.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/apps')
            }
        )
}

module.exports = {
    getApps,
    getFormApp,
    editApp,
    insertApp,
    deleteApp
}