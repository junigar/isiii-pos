const user = require('../../models/security/sec_usersModel')
const { checkPermission } = require('../../userPermission')
const { oneToMany } = require('../../util/queryConstructor')
const bcrypt = require('bcrypt')

const listUsers = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    let userPermissions;
    let formUserPermission;
    checkPermission(route, login)
        .then(
            (result) => {
                if (!result.rows)
                    throw new Error('Grid Route does not exist into DB')
                else {
                    userPermissions = result.rows[0]
                    if (!userPermissions.priv_access)
                        throw new Error('Not allowed to get.')
                    else
                        return checkPermission(`${route}/form`, login)
                }
            })
        .then(
            (result) => {
                if (!result)
                    throw new Error('Form Route not found.')
                else {
                    formUserPermission = result.rows[0]
                    return user.getAllUsers()
                }
            })
        .then(
            (result) => {
                if (result) {
                    for (let row of result.rows) {
                        row.login_encoded = encodeURIComponent(row.login)
                    }
                    let action = req.query.action
                    let returnParams = {
                        resultSet: result.rows,
                        permissions: formUserPermission,
                        app_type: 'grid'
                    }

                    if (action) {
                        returnParams.action = action
                    }
                    res.render('security/sec_users/grid_sec_users', returnParams)
                }
                else
                    throw new Error('No rows selected.')
            })
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/users')
            })
}

const editUser = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const loginParam = decodeURIComponent(req.params.login)
    let userPermissions;
    let userGroupsObj = oneToMany('group_\\d+', req.body)




    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                if (!result)
                    throw new Error('Application grid not found into DB')
                else {
                    userPermissions = result.rows[0]
                    if (!userPermissions.priv_access)
                        throw new Error('Not allowed to get.')
                    else if (!userPermissions.priv_update)
                        throw new Error('Not allowed to update.')
                    else {
                        const toUpdateUser = new user.User(loginParam, undefined, req.body.active, req.body.email, req.body.name)
                        return user.updateUser(toUpdateUser)
                    }
                }
            }
        )
        .then(
            (result) => {
                if (result) {
                    return user.userGroupsModel.deleteUserGroup(loginParam)
                } else
                    throw new Error('Error updating user.')
            }
        )
        .then(
            (result) => {
                if (result) {
                    return user.userGroupsModel.insertUserGroup(loginParam, userGroupsObj)
                } else
                    throw new Error('Error deleting user groups.')
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/users?action=update')
                else
                    throw new Error('No row updated.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/users')
            }
        )
}

const insertUser = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    let userPermissions;
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                if (!result)
                    throw new Error('Application form not found into DB.')
                else {
                    userPermissions = result.rows[0]
                    if (!userPermissions.priv_access)
                        throw new Error('Not allowed to get.')
                    else if (!userPermissions.priv_insert)
                        throw new Error('Not allowed to insert.')
                    else {
                        const passwordsMatch = req.body.password == req.body.password2
                        if (!passwordsMatch)
                            throw new Error('Passwords does not match.')
                        else {
                            return bcrypt.hash(req.body.password, 10);
                        }
                    }
                }
            }
        )
        .then(
            (hashedPassword) => {
                if(hashedPassword){
                    const toInsertUser = new user.User(req.body.login, hashedPassword, req.body.active, req.body.email, req.body.name)
                    return user.insertUser(toInsertUser)
                }else
                    throw new Error('Error hashing password.')
            }
        )
        .then(
            (result) => {
                if(!result)
                    throw new Error('Error inserting user.')
                else{
                    const userGroupsObj = oneToMany('group_\\d+', req.body)
                    return user.userGroupsModel.insertUserGroup(req.body.login, userGroupsObj)
                }
            }
        )
        .then(
            (result) => {
                if(!result)
                    throw new Error('Error inserting user groups')
                else{
                    res.redirect('/users?action=insert')
                }
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/users')
            }
        )
}

const getFormUser = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const loginParam = decodeURIComponent(req.params.login)
    let returnObj = {};
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                if (!result)
                    throw new Error('Application form not found into DB.')
                else {
                    returnObj.permissions = result.rows[0]
                    if (!returnObj.permissions.priv_access)
                        throw new Error('Not allowed to get.')
                    if (loginParam) {
                        if (returnObj.permissions.priv_update)
                            return user.getUserByLogin(loginParam)
                        else
                            throw new Error('Not allowed to update.')
                    } else {
                        if (returnObj.permissions.priv_insert)
                            return user.getUserByLogin(loginParam)
                        else
                            throw new Error('Not allowed to insert.')
                    }
                }
            }
        )
        .then(
            (result) => {
                let userLogin = '';
                if (result) {
                    if (result.rows[0]) {
                        returnObj.resultSet = result.rows[0]
                        returnObj.resultSet.login_encoded = encodeURIComponent(returnObj.resultSet.login)
                        userLogin = returnObj.resultSet.login

                    }
                }
                return user.userGroups(userLogin)
            }
        )
        .then(
            (result) => {
                if (result) {
                    if (result.rows.length > 0) {
                        returnObj.userGroups = result.rows
                    }
                    else
                        throw new Error('Users or Groups undefined.')
                } else {
                    throw new Error('Query error.')
                }
                res.render('security/sec_users/form_sec_users', returnObj)
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/users')
            }
        )
}

const deleteUser = (req, res) => {
    const route = req.baseUrl ? req.baseUrl : req.path
    const login = req.user.login
    const loginParam = decodeURIComponent(req.params.login)
    let userPermissions;
    checkPermission(`${route}/form`, login)
        .then(
            (result) => {
                if (!result)
                    throw new Error('Form Application not found into DB.')
                else {
                    userPermissions = result.rows[0]
                    if (!userPermissions.priv_access)
                        throw new Error('Not allowed to get.')
                    else if (!userPermissions.priv_delete)
                        throw new Error('Not allowed to delete.')
                    else {
                        const toDeleteUser = new user.User(loginParam)
                        return user.deleteUser(toDeleteUser)
                    }
                }
            }
        )
        .then(
            (result) => {
                if (result)
                    res.redirect('/users?action=delete')
                else
                    throw new Error('No row deleted.')
            }
        )
        .catch(
            (e) => {
                console.log(e)
                res.redirect('/users')
            }
        )
}

module.exports = {
    listUsers,
    getFormUser,
    editUser,
    insertUser,
    deleteUser
}