# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* FPUNE - ISIII. Proyecto POS desarrollado por Junior Garozzo y Luna Cañiza

### How do I get set up? ###

* Importar Base de Datos y sus registros de prueba utilizando el archivo db.sql
* Configurar el servidor usando de referencia .env.model donde se especifican las variables globales a utilizar.
    1. NODE_ENV *production* o *development*
    2. PORT puerto en donde se quiere ejecutar el proyecto. Se recomienda el puerto 3000
    3. SESSION_SECRET cadena de caracteres aleatorios para generar sesiones cifradas. Puede ser cualquier cosa
    4. DB_USER usuario de la base de datos. Por defecto: postgres
    5. DB_HOST host donde esta alojada la BD. Por defecto: localhost
    6. DB_PASSWORD contraseña de la BD
    7. DB_DATABASE nombre de la base de datos
    