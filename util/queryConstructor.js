const update = (table, pkObj, toUpdateColumns) => {
    let query = ['UPDATE', `${table}`, 'SET'];
    let pkNameArray = []
    let pkValuesArray = []
    let rowsValue = []
    let conditionalArray = []
    let set = [];
    let count = 0;

    for (let property in pkObj) {
        if (pkObj[property]) {
            pkNameArray.push(property)
            pkValuesArray.push(pkObj[property])
        }
    }

    for (let property in toUpdateColumns) {
        if (toUpdateColumns[property] && !pkNameArray.includes(property, 0)) {
            count++
            set.push(`${property} = (\$${count})`)
            rowsValue.push(toUpdateColumns[property])
        }
    }
    query.push(set.join(', '));
    query.push('WHERE')
    for (let i = 0; i < pkNameArray.length; i++) {
        count++
        conditionalArray.push(`${pkNameArray[i]} = \($${count})`);
    }
    query.push(conditionalArray.join(' AND '))
    
    pkValuesArray.forEach((item) => {
        rowsValue.push(item)
    })
    return {
        query: query.join(' '),
        rowsValue
    }

}

const insertValues = (table, toInsertColumns, seq) => {
    let query = ['INSERT INTO', `${table}`, '('];
    let columns = []
    let parammeters = []
    let aux = []

    for (let property in toInsertColumns) {
        if (toInsertColumns[property] && property != seq) {
            columns.push(property)
            parammeters.push(toInsertColumns[property])
        }
    }
    query.push(columns.join(', '));
    query.push(') VALUES (')
    for (let i = 0; i < columns.length; i++) {
        aux.push(`\$${i + 1}`)
    }
    query.push(aux.join(', '))
    query.push(')')
    return {
        query: query.join(' '),
        parammeters
    }
}

const oneToMany = (expected, objParam) => {
    let expression = new RegExp(expected, 'g')
    let toReturn = {}
    for (let property in objParam) {
        let a = property.match(expression)
        if (a) {
            let b = a[0].match(/\d+/g)
            if (b) {
                toReturn[b] = objParam[property]
            }
        }
    }
    return toReturn
}


module.exports = {
    update,
    insertValues,
    oneToMany
}