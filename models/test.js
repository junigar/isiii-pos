const {pool} = require('../pg-config')

class test{
    constructor(id, nombre){
        this.id = id;
        this.nombre = nombre;
    }
}

const getAll = ()=>{
    return pool.query('SELECT id, nombre FROM public.test');
}



module.exports = {
    getAll, 
    test
}