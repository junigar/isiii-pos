const { pool } = require('../pg-config')
const { update } = require('../util/queryConstructor')

const user = class {
    constructor(login, password, email, name, active) {
        this.login = login
        this.password = password
        this.email = email
        this.active = active
        this.name = name
    }
}


const getAllUsers = () => {
    const queryStr = 'SELECT login, "password", active, email, "name" FROM sec_users';
    return pool.query(queryStr);
}

const getUserbyLogin = async (login) => {
    try {
        let queryStr = 'SELECT login, "password", active, email, "name" FROM sec_users WHERE login = $1';
        let res = await pool.query(queryStr, [login]);
        return res.rows[0];
    } catch (error) {
        console.log(error)
    }
}

const getUserbyEmail = async (email) => {
    try {
        let queryStr = 'SELECT login, "password", active, email, "name" FROM sec_users WHERE email = $1';
        let queryParams = [email];
        let res = await pool.query(queryStr, queryParams);
        return res.rows[0];
    } catch (error) {
        console.log(error)
    }
}

const insertUser = async (userObj) => {
    try {
        let paramsArray = [userObj.login, userObj.password, userObj.active, userObj.email, userObj.name];
        let queryStr = 'INSERT INTO sec_users (login, "password", active, email, "name") VALUES ($1, $2, $3, $4, $5)';
        let res = await pool.query(queryStr, paramsArray);
        return res;
    } catch (error) {
        console.log(error)
    }
}

const updateUser = async (userObj) => {
    try {
        let constructorResponse = update('public.sec_users', 'login', userObj.login, userObj)
        let res = await pool.query(constructorResponse.query, constructorResponse.parammeters);
        return res;
    } catch (error) {
        console.log(error)
    }
}


module.exports = {
    getAllUsers,
    getUserbyLogin,
    getUserbyEmail,
    insertUser,
    updateUser,
    user
}