const { pool } = require('../../pg-config')



const deleteUserGroup = (login) => {
    const queryStr = 'DELETE FROM public.sec_users_groups WHERE login = $1';
    return pool.query(queryStr, [login])
}

const insertUserGroup = (login, valuesObj) => {
    let query = ['INSERT INTO public.sec_users_groups (login, id_group) VALUES']
    let values = []
    for (let property in valuesObj) {
        if (valuesObj[property] != '0') {
            values.push(`('${login}', ${property})`)
        }
    }
    query.push(values.join(', '))
    return pool.query(query.join(' '));
}

module.exports = {
    deleteUserGroup,
    insertUserGroup
}