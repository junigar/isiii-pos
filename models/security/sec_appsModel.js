const { pool } = require('../../pg-config')
const { update, insertValues } = require('../../util/queryConstructor')

const App = class {
    constructor(app_name, description, is_door, type) {
        this.app_name = app_name
        this.description = description
        this.is_door = is_door
        this.type = type
    }
}


const getAllApps = () => {
    const queryStr = 'SELECT * FROM public.sec_apps';
    return pool.query(queryStr);
}

const getAppByName = (app_name) => {
    let queryStr = 'SELECT * FROM public.sec_apps WHERE app_name = $1';
    return pool.query(queryStr, [app_name]);
}

const insertApp = (appObj) => {
    let constructorResult = insertValues('public.sec_apps', appObj)
    return pool.query(constructorResult.query, constructorResult.parammeters);
}

const updateApp = (appObj) => {
    let constructorResult = update('public.sec_apps', 'app_name', appObj.app_name, appObj)
    return pool.query(constructorResult.query, constructorResult.parammeters);
}

const deleteApp = (appObj) => {
    let queryStr = `DELETE FROM public.sec_apps WHERE app_name = $1`;
    let paramsArray = [appObj.app_name]
    return pool.query(queryStr, paramsArray);
}

module.exports = {
    App,
    getAllApps,
    getAppByName,
    insertApp,
    updateApp,
    deleteApp
}