const { pool } = require('../../pg-config')
const { update, insertValues } = require('../../util/queryConstructor')
const userGroupsModel = require('../../models/security/sec_users_groupsModel')

const User = class {
    constructor(login, password, active, email, name) {
        this.login = login
        this.password = password
        this.active = active
        this.email = email,
            this.name = name
    }
}

const getAllUsers = () => {
    const queryStr = 'SELECT * FROM public.sec_users';
    return pool.query(queryStr);
}

const getUserByLogin = (login) => {
    let queryStr = 'SELECT * FROM public.sec_users WHERE login = $1';
    return pool.query(queryStr, [login]);
}

const getUserByEmail = (email) => {
    let queryStr = 'SELECT * FROM public.sec_users WHERE email = $1';
    return pool.query(queryStr, [email]);
}

const insertUser = (userObj) => {
    let constructorResult = insertValues('public.sec_users', userObj)
    return pool.query(constructorResult.query, constructorResult.parammeters);
}

const updateUser = (userObj) => {
    let constructorResult = update('public.sec_users', {'login': userObj.login}, userObj)
    
    return pool.query(constructorResult.query, constructorResult.rowsValue);
}

const deleteUser = (userObj) => {
    let queryStr = `DELETE FROM public.sec_users WHERE login = $1`;
    let paramsArray = [userObj.login]
    return pool.query(queryStr, paramsArray);
}

const userGroups = (login) => {
    const queryStr = `SELECT * FROM fn_user_groups($1)`;
    const paramsArray = [login]
    return pool.query(queryStr, paramsArray);
}

module.exports = {
    User,
    getAllUsers,
    getUserByLogin,
    getUserByEmail,
    insertUser,
    updateUser,
    deleteUser,
    userGroupsModel,
    userGroups
}