const { pool } = require('../../pg-config')
const { update, insertValues } = require('../../util/queryConstructor')

const Group = class {
    constructor(id_group, description) {
        this.id_group = id_group
        this.description = description
    }
}


const getAllGroups = () => {
    const queryStr = 'SELECT id_group, description FROM sec_groups';
    return pool.query(queryStr);
}

const getGroupById = (id_group) => {
    let queryStr = 'SELECT id_group, description FROM sec_groups WHERE id_group = $1';
    return pool.query(queryStr, [id_group]);
}


const insertGroup = (groupObj) => {
    let constructorResult = insertValues('public.sec_groups', groupObj, 'id_group')
    return pool.query(constructorResult.query, constructorResult.parammeters);
}

const updateGroup = (groupObj) => {
    let constructorResult = update('public.sec_groups', {'id_group': groupObj.id_group}, groupObj)
    return pool.query(constructorResult.query, constructorResult.rowsValue);
}

const deleteGroup = (groupObj) => {
    let queryStr = `DELETE FROM public.sec_groups WHERE id_group = $1`;
    let paramsArray = [groupObj.id_group]
    return pool.query(queryStr, paramsArray);
}

module.exports = {
    Group,
    getAllGroups,
    getGroupById,
    insertGroup,
    updateGroup,
    deleteGroup
}