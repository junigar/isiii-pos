const { pool } = require('../../pg-config')
const { update } = require('../../util/queryConstructor')
const { getAllGroups } = require('../../models/security/sec_groupsModel')

const getGroupsAppsByIdGroup = (id_group) => {
    const query = `SELECT sa.description, sa."type", sga.*
                   FROM sec_groups_apps sga
                   JOIN sec_apps sa on sa.app_name = sga.app_name
                   WHERE sga.id_group = $1
                   AND sa.is_door`
    return pool.query(query, [id_group])
}

const updateGroupApp = (reqBody, idGroup) => {
    let permissionsObj = getPermissionsFromForm(reqBody)
    let promisesArray = []
    for (let app_name in permissionsObj) {
        let set = []
        let query = ['UPDATE', 'public.sec_groups_apps', 'SET']
        for(let column in permissionsObj[app_name]){
            set.push(`priv_${column} = '${permissionsObj[app_name][column]}'`)
        }
        query.push(set.join(', '))
        query.push('WHERE')
        query.push(`app_name = '${app_name}' AND id_group = ${idGroup}`)
        
        let constructedQuery = query.join(' ')

        promisesArray.push(pool.query(constructedQuery))
    }
    return Promise.all(promisesArray)
}

const getPermissionsFromForm = (bodyObj) => {
    let toReturnObj = {}
    let auxObj = {}
    for (let property in bodyObj) {
        if (bodyObj[property]) {
            let column = property.match(/\w+_/g)
            column = column[0].replace(/_/g, '')

            let app_name = property.match(/_.+/)
            app_name = app_name[0].replace(/_/, '')


            if (!toReturnObj.hasOwnProperty(app_name)) {
                toReturnObj[app_name] = undefined
                auxObj = {}
            }
            auxObj[column] = bodyObj[property]
            toReturnObj[app_name] = auxObj
        }
    }
    return toReturnObj
}

module.exports = {
    getAllGroups,
    getGroupsAppsByIdGroup,
    updateGroupApp
}