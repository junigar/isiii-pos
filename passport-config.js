const localStrategy = require('passport-local').Strategy
const bcrypt = require('bcrypt')

function initialize(passport, getUserByEmail, getUserByLogin) {
    const authenticateUser = async (email, password, done) => {
        try {
            const user = await getUserByEmail(email)
            // console.log(user)
            if (user == null) {
                return done(null, false, { message: 'No user with that email' })
            }
            try {
                if (await bcrypt.compare(password, user.password)) {
                    if (!user.active) {
                        return done(null, false, { message: "Account disabled" })
                    }
                    return done(null, user)
                } else {
                    return done(null, false, { message: 'Password incorrect' })
                }
            } catch (error) {
                return done(error)
            }
        } catch (error) {
            console.log(error)
        }

    }
    passport.use(new localStrategy({
        usernameField: 'email'
    }, authenticateUser))
    passport.serializeUser((user, done) => done(null, user.login))
    passport.deserializeUser(async (login, done) => {
        return done(null, await getUserByLogin(login))

    })

}

module.exports = initialize

