const { pool } = require('../pg-config')

const eventHandler = (req, res, next) => {
    let route = '';
    console.log(req.params)
    if(req.baseUrl === ''){
        route = req.path
    }else{
        route = req.baseUrl
    }
    const checkPermission = ()=> {
        const queryStr = `SELECT * 
                          FROM vw_user_permissions 
                          WHERE app_name = $1 
                          AND login = $2 LIMIT 1`;
        return pool.query(sql, [route, req.user.login])
    }

    checkPermission().then((result) => {
        const filas = result.rows
        for (let i = 0; i < filas.length; i++) {
            if (filas[i].priv_access) {
                return next()
            }
        }
        res.redirect('/error');
    });

}

module.exports = {
    eventHandler
}