
const deleteFromGrid = () => {
    let forms = document.querySelectorAll('form');

    forms.forEach((form, index) => {
        let deleteIcon = form.querySelector('i.delete-icon')

        deleteIcon.addEventListener('click', () => {
            Swal.fire({
                title: 'System',
                text: "Are you sure you want to delete?",
                icon: 'warning',
                showCancelButton: true
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit()
                }
            })
        })
    })
}