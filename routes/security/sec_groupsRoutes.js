const express = require('express')
const  router = express.Router();
const groupsController = require('../../controllers/security/sec_groupsController')


router.get('/', groupsController.getGroups)
router.get('/form', groupsController.getFormGroup)
router.post('/form', groupsController.insertGroup)
router.get('/form/:id_group', groupsController.getFormGroup)
router.put('/form/:id_group', groupsController.editGroup)
router.delete('/form/:id_group', groupsController.deleteGroup)
    

module.exports = router;
