const express = require('express')
const  router = express.Router();
const usersController = require('../../controllers/security/sec_usersController')


router.get('/', usersController.listUsers)
router.get('/form', usersController.getFormUser)
router.post('/form', usersController.insertUser)
router.get('/form/:login', usersController.getFormUser)
router.put('/form/:login', usersController.editUser)
router.delete('/form/:login', usersController.deleteUser)

module.exports = router;
