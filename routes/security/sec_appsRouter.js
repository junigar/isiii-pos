const express = require('express')
const  router = express.Router();
const appsController = require('../../controllers/security/sec_appsController')


router.get('/', appsController.getApps)
router.get('/form', appsController.getFormApp)
router.post('/form', appsController.insertApp)
router.get('/form/:app_name', appsController.getFormApp)
router.put('/form/:app_name', appsController.editApp)
router.delete('/form/:app_name', appsController.deleteApp)
    

module.exports = router;
