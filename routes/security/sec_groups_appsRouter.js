const express = require('express')
let  router = express.Router();
const groupsAppsController = require('../../controllers/security/sec_groups_appsController')


router.get('/', groupsAppsController.selectGroup)
router.post('/', groupsAppsController.redirToList)
router.get('/:id_group', groupsAppsController.listGroupsAppsByIdGroup)
router.post('/:id_group', groupsAppsController.updatePermissions)

    

module.exports = router